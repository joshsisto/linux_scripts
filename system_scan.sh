echo gathering system info

VAR_TIME=$(date +%F_%H:%M:%S:%N | sed 's/\(:[0-9][0-9]\)[0-9]*$/\1/')

SYSTEM_IP=$(ifconfig | grep "inet " | grep -Fv 127.0.0.1 | awk '{print $2}')

PUBLIC_IP=$(curl 'https://api.ipify.org?')

echo "Your local IP address is $SYSTEM_IP" > $VAR_TIME.txt

echo "Your public IP address is $PUBLIC_IP" >> $VAR_TIME.txt

uptime -p >> $VAR_TIME.txt

cat /proc/version >> $VAR_TIME.txt

cat /proc/cpuinfo >> $VAR_TIME.txt

sudo fdisk -l >> $VAR_TIME.txt

sudo lshw -class disk >> $VAR_TIME.txt

sudo smartctl --all /dev/sda >> $VAR_TIME.txt

read -p "Would you like to scan the local network?" yn
case $yn in
    [Yy]* ) nmap $SYSTEM_IP/24 -oA $VAR_TIME;;
    [Nn]* ) echo exit;;
    * ) echo "Please answer yes or no.";;
esac


read -p "Would you like to scan your disk for errors? (This could take a long time...)" yn
case $yn in
    [Yy]* ) sudo badblocks -v /dev/sda > badsectors.txt;;
    [Nn]* ) echo exit;;
    * ) echo "Please answer yes or no.";;
esac
