#!/bin/bash

# A script to detect if I am using my home public IP address or if I am using a VPN

# home_IP runs an nslookup command against the DDNS service provided by my Mikrotik firewall
home_IP=$(nslookup ddns.mikrotik.com | grep Address: | tail -1)

# get your public IP address using curl command
your_current_IP=$(curl --silent ifconfig.me)

# remove Address: from IP to compare IP's
home_IP_plus=${home_IP#"Address: "}

echo "Your home IP address is $home_IP_plus"
echo "Your current public IP address is $your_current_IP"

if [ $home_IP_plus = $your_current_IP ]
	then
		echo "You are not using a VPN!"
	else
		echo "Home IP address not detected. You are most likely using a VPN."
fi

