#!/bin/bash

/usr/bin/expect <(cat << EOF
spawn sudo openvpn --config ca-aes-256-gcm-udp-dns.ovpn
expect "Enter Auth Username:"
send "PIAUSERNAME\r"
expect "Enter Auth Password: (press TAB for no echo)"
send "PIAPASSWORD\r"
interact
EOF
)
